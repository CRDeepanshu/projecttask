import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DannyComponent } from './danny/danny.component';
import { JmitComponent } from './jmit/jmit.component';

@NgModule({
  declarations: [
    AppComponent,
    DannyComponent,
    JmitComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
